const Home =  {
    HCM: 'HCM',
    HN: 'HN',
    DN: 'DN'
}
const key = {
    code: "5fb61fd19922248eb6e530c47c2fd7ed"
}
const url = (id) => {
    return  "http://api.openweathermap.org/data/2.5/weather?id="+id+"&appid=5fb61fd19922248eb6e530c47c2fd7ed"
}
const iconWeather = (icon) => {
    return ' http://openweathermap.org/img/wn/'+icon+'.png'
}

module.exports = {
    Home,
    url,
    iconWeather
}