import React from 'react';
import { WebView } from 'react-native-webview';
 
// ...
const ShowView = (props) => {
    
    return <WebView source={{ uri: 'https://reactnative.dev/' }} />;
}

export default ShowView