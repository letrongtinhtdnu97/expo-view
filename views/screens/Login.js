import React from 'react'
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native'
import { TextInput,Button ,Avatar,Snackbar } from 'react-native-paper';
import axios from 'axios'
const Login = (props) => {
    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [visible, setVisible] = React.useState(false);
    const [error, setError] = React.useState('');
    const handleSubmit = async() => {
        const body = {
            username,
            password
        }
        // try {
        //     const results = await axios
        //     .post('http://localhost:3000/post-user',body)
        //     if(results.data.code === 0) {
        //         return props.navigation.navigate("Home")
        //     } else {
        //         setError("Tài khoản không đúng")
        //         return setVisible(true)
        //     }
        // } catch (error) {
        //     setError("Tài khoản không đúng")
        //     return setVisible(true)
        // }
        return props.navigation.navigate("Home")
    }
    return(
        <View style={{
            flex: 1,
            flexDirection:'column', 
            alignItems:'center', 
            justifyContent:'space-around'}}>
            <View style={{
                flexDirection:'column', 
                alignItems:'center'
            }} >
                <Avatar.Text size={160} label="App" />
                <Text style={{paddingTop:20, fontSize:20}} >
                    Welcome Login
                </Text>
            </View>
            
            <View style={{paddingBottom:160}}>
                <TextInput
                    mode="outlined"
                    style={styles.text}
                    placeholder="Tài khoản"
                    label="Tài khoản"
                    value={username}
                    onChangeText={text => setUsername(text)}
                />
                <TextInput
                    secureTextEntry={true}
                    mode="outlined"
                    style={styles.text}
                    placeholder="Mật khẩu"
                    label="Mật khẩu"
                    value={password}
                    onChangeText={text => setPassword(text)}
                />
                <View style={{paddingTop:10}}>
                    <Button 
                        icon="login" 
                        mode="contained" 
                        onPress={handleSubmit}
                        disabled={visible}
                    >
                        Đăng nhập
                    </Button>
                </View>
                <View style={{paddingTop:50}}>
                <Button 
                    
                    icon="camera" 
                    mode="text" 
                    onPress={() => props.navigation.navigate('Register')}>
                    Đăng kí
                </Button>

                </View>
            </View>
            <Snackbar
                visible={visible}
                onDismiss={()=>setVisible(false)}
                action={{
                label: 'Cancle',
                onPress: () => {
                    setVisible(false)
                },
                }}>
                {error}
        </Snackbar>
        </View>
    );
}
const styles = StyleSheet.create({
    text: {
        height:45
    }
})
export default Login;
