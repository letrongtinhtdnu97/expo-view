import React, {useState, useEffect} from 'react'
import {View, StyleSheet, Text, TouchableOpacity, Alert} from 'react-native'
import { TextInput,Button, Snackbar  } from 'react-native-paper';
import { AntDesign } from '@expo/vector-icons'; 

const Register = (props) => {
    const [name, setName] = useState('');
    const [idname, setIDname] = useState("");
    const [pass, setPass] = useState("");
    const [repass, setRePass] = useState("");
    const [visible, setVisible] = useState(false)
    const [error ,setError] = useState("")
    
    const handleSubmit = async () => {
       if(!name ||!idname ||!pass || !repass  ) {
           setError("Vui lòng nhập đầy đủ thông tin")
           return setVisible(true)
       }
       if(pass !==repass) {
        setError("Mật khẩu không trùng khớp")
        return setVisible(true)
       }
       const body = {
           name,
           idname,
           pass,
           repass
       }
       console.log(body)
       fetch('http://localhost:3000/post-user',
       {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: body
      })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.code === 0) {
            setError("Tạo tài khoản thành công")
            return setVisible(true) 
        }
      })
      .catch((error) => {
        setError("Không thể tạo tại khoản bằng tên đăng nhập này")
        return setVisible(true)
      });
    }
    return(
        <>
        <View style={{padding:20}}>
            <TouchableOpacity onPress={()=>props.navigation.goBack()}>
                <AntDesign 
                name="leftcircleo" 
                size={34} 
                color="black" />
            </TouchableOpacity>
       
        </View>
        <View style={{
            flex: 1,
            flexDirection:'column', 
            alignItems:'center', 
            justifyContent:'space-around'}}>
            <View style={{
                flexDirection:'column', 
                alignItems:'center'
            }} >
                
                <Text style={{paddingTop:20, fontSize:20}} >
                    Welcome đăng kí
                </Text>
            </View>
            
            <View style={{paddingBottom:160}}>
                <Text>Họ và Tên</Text>
                <TextInput
                    mode="outlined"
                    style={styles.text}
                    placeholder="Le Van A"
                    
                    value={name}
                    onChangeText={text => setName(text)}
                />
                <Text>Tài khoản</Text>
                <TextInput
                    mode="outlined"
                    style={styles.text}
                    placeholder="levana"
                    value={idname}
                    onChangeText={text => setIDname(text)}
                />
                <Text>Mật khẩu</Text>
                <TextInput
                    secureTextEntry={true}
                    mode="outlined"
                    style={styles.text}
                    placeholder="123.."
                    value={pass}
                    onChangeText={text => setPass(text)}
                />
                <Text>Nhập lại mật khẩu</Text>
                <TextInput
                    secureTextEntry={true}
                    mode="outlined"
                    style={styles.text}
                    placeholder="123.."
                    value={repass}
                    onChangeText={text => setRePass(text)}
                />
                <View style={{paddingTop:10}}>
                    <Button 
                        icon="login" 
                        mode="contained" 
                        onPress={handleSubmit}
                        disabled={visible}
                    >
                        Đăng kí
                    </Button>
                </View>
                <View style={{paddingTop:50}}>
                <Button 
                    disabled={visible}
                    icon="camera" 
                    mode="text" 
                    onPress={() => console.log('Pressed')}>
                    Quay lại login
                </Button>

                </View>
            </View>
        </View>
        <Snackbar
            visible={visible}
            onDismiss={()=>setVisible(false)}
            action={{
            label: 'Cancle',
            onPress: () => {
                setVisible(false)
            },
            }}>
            {error}
        </Snackbar>
        </>
    );
}
const styles = StyleSheet.create({
    text: {
        height:45
    }
})
export default Register;
