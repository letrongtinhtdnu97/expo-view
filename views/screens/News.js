import React from 'react'
import {View, FlatList,SafeAreaView } from 'react-native'
import CardView from '../components/CardView'
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import {DATA1} from '../mocks/title'
 const News = (props) => {
    return(
       <SafeAreaView style={{flex:1}}>
            <FlatList
            data={DATA1}
            renderItem={({item}) => 
                <View style={{padding:10}}>
                    <CardView navigation={props.navigation}  data ={item} />
                </View>
            }
            keyExtractor={item => item.id}
        />
       </SafeAreaView>
    );
}

export default News;
