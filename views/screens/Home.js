import React from 'react'
import {View, FlatList,SafeAreaView, Text } from 'react-native'
import CardView from '../components/CardView'

import {DATA} from '../mocks/title'
import {url} from '../constants/screens'

 const Login = (props) => {
    console.log(url(1))
    return(
       <SafeAreaView style={{flex:1}}>
           <View style={{
               height:50,
               backgroundColor:'rgb(98, 0, 238)',
               flexDirection:'row',
               alignItems:"center",
               justifyContent:'center'
           }}>
               <Text style={{color:'white', fontSize:20}}>TRANG CHỦ</Text>
            </View>
            <FlatList
            data={DATA}
            renderItem={({item}) => 
                <View style={{padding:10}}>
                    <CardView navigation={props.navigation} data ={item} />
                </View>
            }
            keyExtractor={item => item.id}
        />
       </SafeAreaView>
    );
}

export default Login;
