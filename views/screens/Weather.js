import React,{useState,useEffect} from 'react'
import {Text, View, Image,TouchableOpacity} from 'react-native'
import { Card, Title, Paragraph, Icon } from 'react-native-paper';
import axios from 'axios';
import {url,iconWeather} from '../constants/screens'
const hn = require('../../assets/hn.jpg')
const hcm = require('../../assets/hcm.jpg')
const dn = require('../../assets/dn.jpg')
import { AntDesign } from '@expo/vector-icons'; 
const Weather = (props) => {
    
    const {data} = props.route.params
    const [image, setImage] = useState('')
    const [data1 , setData1] = useState({})
    const [arr, setArr] = useState([])
    const [icon1, setIcon] = useState('')
    const [desc , setDesc] = useState('')
    useEffect(() => {
        getApiWeather()
        
        if(data?.id === '1') {
            setImage(hn)
        }
        if(data?.id === '2') {
            setImage(hcm)
        }
        if(data?.id === '3') {
            setImage(dn)
        }
    }, [])
    const LeftContent = props => <Image resizeMode="contain" style={{height:40, width:40}} source={{uri: icon1?  iconWeather(icon1) : ''}} />
    
    const getApiWeather = async() => {
        try {
            const api =  await axios.get(url(data.address))
            
            setData1(api.data)
        } catch (error) {
            
        }
    }
    useEffect(() => {
        setArr(data1.weather ? data1.weather : [])
        
        arr.forEach((item) => {
            
            setDesc(item.description)
        })
        
    },[data1])
    useEffect(() => {
        
        setIcon(arr[0]?.icon ? arr[0]?.icon : '')
        setDesc( arr[0]?.description ? arr[0]?.description : '')
    }, [arr])
    
    
    return(
        <View>
            <View style={{
               height:50,
               backgroundColor:'rgb(98, 0, 238)',
               flexDirection:'row',
               alignItems:"center",
               justifyContent:"space-between"
           }}><TouchableOpacity style={{paddingLeft:20}} onPress={()=>props.navigation.goBack()}>
                    <AntDesign 
                    name="leftcircleo" 
                    size={24} 
                    color="black" />
                </TouchableOpacity>
               <Text style={{color:'white', fontSize:20}}>{data.name}</Text>
               <View></View>
            </View>
            <View style={{justifyContent:'center', backgroundColor:'red'}}>
                <Card>
               
                <Card.Cover source={{ uri: image }} />
                <Card.Actions>
                <Card.Content>
                <Title>{data.name}</Title>
                    <Paragraph>{desc} </Paragraph>
                </Card.Content>
                </Card.Actions>
                </Card>
            </View>
            <View style={{paddingTop:10}}>
                <Card>
            <Card.Title title="Sức gió" subtitle="meter/sec" left={LeftContent} right={()=><Text style={{paddingRight:10}}>{data1?.wind?.speed ? data1?.wind?.speed : 0}</Text>} />
                </Card>
            </View>
            <View style={{paddingTop:10}}>
                <Card>
                <Card.Title title="Mây" subtitle="Cloudiness, %" left={LeftContent} right={()=><Text style={{paddingRight:10}}>{data1?.clouds?.all ? data1?.clouds?.all : 0}</Text>} />
                </Card>
            </View>
            <View style={{paddingTop:10}}>
                <Card>
                <Card.Title title="Áp suất" subtitle="hPa" left={LeftContent} right={()=><Text style={{paddingRight:10}}>{data1?.main?.pressure ? data1?.main?.pressure : 0}</Text>} />
                </Card>
            </View>
            {/* <View style={{paddingTop:10}}>
                <Card>
                <Card.Title title="Nhiệt độ" subtitle="Card Subtitle" left={LeftContent} />
                </Card>
            </View> */}
        </View>
        
    );
}

export default Weather;
