import React , {useState,useEffect}from 'react'
import {View, Text} from 'react-native'
import { AntDesign } from '@expo/vector-icons'; 
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';

const LeftContent = props => <Avatar.Icon {...props} icon={
  ()=>  <AntDesign name="adduser" size={24} color="black" />
} />
const hn = require('../../assets/hn.jpg')
const hcm = require('../../assets/hcm.jpg')
const dn = require('../../assets/dn.jpg')
const h24 = require('../../assets/h24.png')
const qc = require('../../assets/qc.jpg')
const vne = require('../../assets/vne.jpg')
const zing = require('../../assets/zing.png')
const CardView = (props) => {
    const [image, setImage] = useState('')
    useEffect(() => {
        if(props?.data?.id === '1') {
            setImage(hn)
        }
        if(props?.data?.id === '2') {
            setImage(hcm)
        }
        if(props?.data?.id === '3') {
            setImage(dn)
        }
        if(props?.data?.id === '4') {
            setImage(vne)
        }
        if(props?.data?.id === '5') {
            setImage(h24)
        }
        
        if(props?.data?.id === '7') {
            setImage(zing)
        }
        if(props?.data?.id === '8') {
            setImage(qc)
        }
    }, [])
    const handleSubmit = () => {
        if(props?.data?.id === '4' ) {
            return props.navigation.navigate('ShowView',{uri: 'https://vnexpress.net/'})
        }
        if(props?.data?.id === '5' ) {
            return props.navigation.navigate('ShowView',{uri: 'https://www.24h.com.vn/'})
        }
        if(props?.data?.id === '7' ) {
            return props.navigation.navigate('ShowView',{uri: 'https://zingnews.vn/'})
        }
        if(props?.data?.id === '8' ) {
            return props.navigation.navigate('ShowView',{uri: 'https://vnexpress.net/'})
        }
        return props.navigation.navigate('Weather',{data: props.data})
    } 
    return(
    <Card onPress={handleSubmit}>
        <Card.Title 
            title="Admin" 
            subtitle={props?.data?.title ?props?.data?.title : '' } 
            left={LeftContent} 
        />
        
        <Card.Cover source={image} />
        {/* <Card.Actions>
            <Button>Cancel</Button>
            <Button>Ok</Button>
        </Card.Actions> */}
    </Card>
    );
}

export default CardView;
