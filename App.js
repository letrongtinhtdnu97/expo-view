
import 'react-native-gesture-handler';
import {StatusBar} from 'expo-status-bar'
import React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Login from './views/screens/Login'
import Home from './views/screens/Home'
import News from './views/screens/News'
import Weather from './views/screens/Weather'
import Register from './views/screens/Register'
import ShowView from './views/screens/View'
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const TabScreen = () => {
  return(
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="News" component={News} />
    </Tab.Navigator>
  );
}
const  App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen 
          name="Login" 
          component={Login} 
          options={{headerShown: false}}
        />
        <Stack.Screen 
          name="Register" 
          component={Register} 
          options={{headerShown: false}}
        />
        <Stack.Screen 
          name="Home" 
          component={TabScreen} 
          options={{headerShown: false}} 
        />
        <Stack.Screen 
          name="Weather" 
          component={Weather} 
          options={{headerShown: false}} 
        />
         <Stack.Screen 
          name="News" 
          component={News} 
          options={{headerShown: false}} 
        />
        <Stack.Screen 
          name="ShowView" 
          component={ShowView} 
          
        />
        
        
        
        
      </Stack.Navigator>
    </NavigationContainer>
  );
}


export default App;


